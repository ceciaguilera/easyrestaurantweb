﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CajaCrearBoleta.aspx.cs" Inherits="EasyRestaurantWeb.Vistas.Caja.CajaCrearBoleta" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <!-- CSS -->
    <link href="../resources/css/estilos.css" rel="stylesheet" />
    <link href="../Content/site.css" rel="stylesheet" />
    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" />
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</head>
<body>
    <header>
        <!-- Navbar-->
        <nav class="navbar navbar-expand-sm bg-dark navbar-dark fixed-top">
            <a class="navbar-brand" href="../Login.aspx">Easy Restaurant</a>
            <ul class="navbar-nav">
                            <li class="nav-item">
                <a class="nav-link" href="ClienteOpciones.aspx">Volver</a>
            </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Contacto</a>
                </li>
            </ul>
        </nav>
    </header>
    <form id="form1" runat="server">
        <style>
            /* NOTE: The styles were added inline because Prefixfree needs access to your styles and they must be inlined if they are on local disk! */
            body {
                background: url('https://d1ralsognjng37.cloudfront.net/bbbde3f7-8508-486a-95bf-119bc120662f.jpeg') no-repeat center center fixed;
                -webkit-background-size: cover;
                -moz-background-size: cover;
                background-size: cover;
                -o-background-size: cover;
            }
        </style>
        <br />
        <br />
        <div class="container">
            <div class="card border-0 shadow my-5">
                <div class="card-body p-5">
                    <h1 class="font-weight-light">Revisar Mesas Pendientes de Cobro</h1>
                    <br />
                    <!-- Tabla-->
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Mesa</th>
                                <th>Pedido</th>
                                <th>Precio C/U</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Mesa 1</td>
                                <td>Chorrillana/huevo
                                </td>
                                <th>$5.990
                                </th>
                            </tr>
                            <tr>
                                <td></td>
                                <td>Coca Cola Light
                                </td>
                                <th>$2.000
                                </th>
                            </tr>
                            <tr>
                                <td></td>
                                <td>Coca Cola Light
                                </td>
                                <th>$2.000
                                </th>
                            </tr>
                            <tr>
                                <td></td>
                                <td>TOTAL
                                </td>
                                <th>$9.990
                                </th>
                            </tr>
                             <tr>
                                <td></td>
                                <td>Propina Sugerida (10%)
                                </td>
                                <th>$990
                                </th>
                            </tr>
                             <tr>
                                <td></td>
                                <td>TOTAL FINAL
                                </td>
                                <th>$10.980
                                </th>
                            </tr>
                        </tbody>
                    </table>
                     <div class="col-12 text-center">
                    <asp:Button class="btn btn-secondary" ID="Button1" runat="server" Text="Enviar Boleta a la Mesa" />
                         </div>
                    <div style="height: 200px"></div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
