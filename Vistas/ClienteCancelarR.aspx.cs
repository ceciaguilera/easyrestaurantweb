﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.OracleClient;
using System.Data;
using System.Windows.Forms;

namespace EasyRestaurantWeb.Vistas
{
    public partial class ClienteCancelarR : System.Web.UI.Page
    {
        OracleConnection conexion = new OracleConnection("DATA SOURCE = ORCL ; PASSWORD = rs2020 ; USER ID = restauranteasy");
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                FillTabla();
            }
        }

        private void FillTabla()
        {
            conexion.Open();
            OracleCommand comando = new OracleCommand("SP_GET_RESERVA", conexion);
            comando.CommandType = System.Data.CommandType.StoredProcedure;
            comando.Parameters.Add("registroReserva", OracleType.Cursor).Direction = ParameterDirection.Output;
            OracleDataAdapter adaptador = new OracleDataAdapter();
            adaptador.SelectCommand = comando;
            DataTable tabla = new DataTable();
            adaptador.Fill(tabla);
            gridCancelar.DataSource = tabla;
            gridCancelar.DataBind();

            conexion.Close();
        }

        protected void btnActualizar_Click(object sender, EventArgs e)
        {
            conexion.Open();
            OracleCommand comando = new OracleCommand("SP_GET_RESERVA", conexion);
            comando.CommandType = System.Data.CommandType.StoredProcedure;
            comando.Parameters.Add("registroReserva", OracleType.Cursor).Direction = ParameterDirection.Output;
            OracleDataAdapter adaptador = new OracleDataAdapter();
            adaptador.SelectCommand = comando;
            DataTable tabla = new DataTable();
            adaptador.Fill(tabla);
            gridCancelar.DataSource = tabla;
            gridCancelar.DataBind();

            conexion.Close();
        }

        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            foreach (GridViewRow row in gridCancelar.Rows)
            {
                System.Web.UI.WebControls.CheckBox cbox = (row.Cells[0].FindControl("chkCancelar") as System.Web.UI.WebControls.CheckBox);
                int rollno = Convert.ToInt32(row.Cells[1].Text);
                if (cbox.Checked)
                {
                    deleterow(rollno);
                }
            }
            gridCancelar.DataBind();
        }

        private void deleterow(int rollno)
        {
            String updatedata = "delete from RESERVA where RESERVA_ID = " + rollno;
            OracleCommand cmd = new OracleCommand(updatedata, conexion);
            conexion.Open();
            cmd.CommandText = updatedata;
            cmd.ExecuteNonQuery();
            MessageBox.Show("Reserva Eliminada");
        }
    }
}