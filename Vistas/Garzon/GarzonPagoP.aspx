﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GarzonPagoP.aspx.cs" Inherits="EasyRestaurantWeb.Vistas.Garzon.GarzonPagoP" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <!-- CSS -->
    <link href="../resources/css/estilos.css" rel="stylesheet" />
    <link href="../Content/site.css" rel="stylesheet" />
    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" />
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</head>
<body>
    <header>
        <!-- Navbar-->
        <nav class="navbar navbar-expand-sm bg-dark navbar-dark fixed-top">
            <a class="navbar-brand" href="Login.aspx">Easy Restaurant</a>
            <ul class="navbar-nav">
                            <li class="nav-item">
                <a class="nav-link" href="ClienteOpciones.aspx">Volver</a>
            </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Contacto</a>
                </li>
            </ul>
        </nav>
    </header>
    <form id="form1" runat="server">
        <style>
            /* NOTE: The styles were added inline because Prefixfree needs access to your styles and they must be inlined if they are on local disk! */
            body {
                background: url('https://d1ralsognjng37.cloudfront.net/bbbde3f7-8508-486a-95bf-119bc120662f.jpeg') no-repeat center center fixed;
                -webkit-background-size: cover;
                -moz-background-size: cover;
                background-size: cover;
                -o-background-size: cover;
            }
        </style>
        <br />
        <br />
        <div class="container">
            <div class="card border-0 shadow my-5">
                <div class="card-body p-5">
                    <h1 class="font-weight-light">Revisar Ordenes Pendientes de Pago</h1>
                    <br />
                    <div class="form-inline">
                        <button class="btn btn-secondary" type="submit">Pagos Pedientes</button>
                        <button class="btn btn-warning" type="submit">Pagos Terminados</button>
                    </div>
                    <br />
                    <!-- Tabla-->
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>N° Mesa</th>
                                <th>Detalle Orden</th>
                                <th>Comentarios Extras</th>
                                <th>Forma de Pago Efectivo/Redcompra</th>
                                <th>Confirmar Pago</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Mesa 1</td>
                                <td>- Chorrillana/huevo
                                    - Coca Cola Light
                                    - Coca Cola Sin Azucar
                                </td>
                                <th></th>
                                <th>Efectivo</th>
                                <td>
                                    <!-- Button trigger modal -->
                                    <button type="button" class="btn btn-secondary" data-toggle="modal" data-target="#exampleModal">
                                        Confirmar Pago
                                    </button>
                                    <!-- Modal -->
                                    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">Confirmación de Pago</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    Dirígete a la Mesa para realizar el cobro de forma Presencial.
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div style="height: 200px"></div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
