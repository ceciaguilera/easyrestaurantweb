﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls;
using System.Data.OracleClient;
using System.Windows.Forms;
using System.Data;

namespace EasyRestaurantWeb.Vistas.Garzon
{
    public partial class GarzonPedidoP : System.Web.UI.Page
    {
        OracleConnection conexion = new OracleConnection("DATA SOURCE = ORCL ; PASSWORD = rs2020 ; USER ID = restauranteasy");

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                FillTabla();
            }
        }

        private void FillTabla()
        {
            conexion.Open();
            OracleCommand comando = new OracleCommand("SP_GET_ORDENGARZON", conexion);
            comando.CommandType = System.Data.CommandType.StoredProcedure;
            comando.Parameters.Add("registroOrdenGarzon", OracleType.Cursor).Direction = ParameterDirection.Output;
            OracleDataAdapter adaptador = new OracleDataAdapter();
            adaptador.SelectCommand = comando;
            DataTable tabla = new DataTable();
            adaptador.Fill(tabla);
            gridGarzonPedido.DataSource = tabla;
            gridGarzonPedido.DataBind();
        }

        protected void btnEntregarORden_Click(object sender, EventArgs e)
        {
            foreach (GridViewRow row in gridGarzonPedido.Rows)
            {
                System.Web.UI.WebControls.CheckBox cbox = (row.Cells[0].FindControl("chkGarzonPedido") as System.Web.UI.WebControls.CheckBox);
                int rollno = Convert.ToInt32(row.Cells[1].Text);
                if (cbox.Checked)
                {
                    updateRow(rollno);
                }
            }
            gridGarzonPedido.DataBind();
        }

        private void updateRow(int rollno)
        {
            String updatedata = "UPDATE ORDENES SET ESTADO_ORDEN = 'Entregada' WHERE ID_ORDEN = " + rollno;
            OracleCommand cmd = new OracleCommand(updatedata, conexion);
            conexion.Open();
            cmd.CommandText = updatedata;
            cmd.ExecuteNonQuery();
            MessageBox.Show("Orden Entregada");
        }

        protected void btnOrdenesPendientes_Click(object sender, EventArgs e)
        {
            conexion.Open();
            OracleCommand comando = new OracleCommand("SP_GET_ORDENGARZON", conexion);
            comando.CommandType = System.Data.CommandType.StoredProcedure;
            comando.Parameters.Add("registroOrdenGarzon", OracleType.Cursor).Direction = ParameterDirection.Output;
            OracleDataAdapter adaptador = new OracleDataAdapter();
            adaptador.SelectCommand = comando;
            DataTable tabla = new DataTable();
            adaptador.Fill(tabla);
            gridGarzonPedido.DataSource = tabla;
            gridGarzonPedido.DataBind();
        }

        protected void btnOrdenesTerminadas_Click(object sender, EventArgs e)
        {
            conexion.Open();
            OracleCommand comando = new OracleCommand("SP_GET_ORDENGARZONENT", conexion);
            comando.CommandType = System.Data.CommandType.StoredProcedure;
            comando.Parameters.Add("registroOrdenEnt", OracleType.Cursor).Direction = ParameterDirection.Output;
            OracleDataAdapter adaptador = new OracleDataAdapter();
            adaptador.SelectCommand = comando;
            DataTable tabla = new DataTable();
            adaptador.Fill(tabla);
            gridGarzonPedido.DataSource = tabla;
            gridGarzonPedido.DataBind();
        }
    }
}