﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace EasyRestaurantWeb.Vistas.Garzon
{
    public partial class GarzonOpciones : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnPendEntrega_Click(object sender, EventArgs e)
        {
            Response.Redirect("GarzonPedidoP.aspx");
        }

        protected void btnPagoPend_Click(object sender, EventArgs e)
        {
            Response.Redirect("GarzonPagoP.aspx");
        }
    }
}