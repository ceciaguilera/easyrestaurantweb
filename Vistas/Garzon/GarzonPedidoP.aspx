﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GarzonPedidoP.aspx.cs" Inherits="EasyRestaurantWeb.Vistas.Garzon.GarzonPedidoP" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <!-- CSS -->
    <link href="../resources/css/estilos.css" rel="stylesheet" />
    <link href="../Content/site.css" rel="stylesheet" />
    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" />
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</head>
<body>
    <header>
        <!-- Navbar-->
        <nav class="navbar navbar-expand-sm bg-dark navbar-dark fixed-top">
            <a class="navbar-brand" href="../Login.aspx">Easy Restaurant</a>
            <ul class="navbar-nav">
                            <li class="nav-item">
                <a class="nav-link" href="ClienteOpciones.aspx">Volver</a>
            </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Contacto</a>
                </li>
            </ul>
        </nav>
    </header>
    <form id="form1" runat="server">
        <style>
            /* NOTE: The styles were added inline because Prefixfree needs access to your styles and they must be inlined if they are on local disk! */
            body {
                background: url('https://d1ralsognjng37.cloudfront.net/bbbde3f7-8508-486a-95bf-119bc120662f.jpeg') no-repeat center center fixed;
                -webkit-background-size: cover;
                -moz-background-size: cover;
                background-size: cover;
                -o-background-size: cover;
            }
        </style>
        <br />
        <br />
        <div class="container">
            <div class="card border-0 shadow my-5">
                <div class="card-body p-5">
                    <h1 class="font-weight-light">Revisar Ordenes Pendientes</h1>
                    <br />
                    <br />
                    <!-- Tabla-->
                    <asp:GridView ID="gridGarzonPedido" runat="server" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="4" ForeColor="Black" GridLines="Horizontal" Width="1018px">
                        <Columns>
                            <asp:TemplateField ShowHeader="False">                                 
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkGarzonPedido" runat="server" />
                                </ItemTemplate>                                   
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle BackColor="#CCCC99" ForeColor="Black" />
                        <HeaderStyle BackColor="#333333" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Right" />
                        <SelectedRowStyle BackColor="#CC3333" Font-Bold="True" ForeColor="White" />
                        <SortedAscendingCellStyle BackColor="#F7F7F7" />
                        <SortedAscendingHeaderStyle BackColor="#4B4B4B" />
                        <SortedDescendingCellStyle BackColor="#E5E5E5" />
                        <SortedDescendingHeaderStyle BackColor="#242121" />
                    </asp:GridView>
                    <br />
                    <asp:Button class="btn btn-secondary" ID="btnEntregarORden" runat="server" Text="Entregar Orden" OnClick="btnEntregarORden_Click"  />
                    <asp:Button class="btn btn-warning" ID="btnOrdenesPendientes" runat="server" Text="Ordenes Pedientes" OnClick="btnOrdenesPendientes_Click"  />
                    <asp:Button class="btn btn-warning" ID="btnOrdenesTerminadas" runat="server" Text="Ordenes Entregadas" OnClick="btnOrdenesTerminadas_Click"  />
                    <br />
                    <div style="height: 200px"></div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
