﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ClienteReservar.aspx.cs" Inherits="EasyRestaurantWeb.Vistas.ClienteReservar" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <!-- CSS -->
    <link href="../resources/css/estilos.css" rel="stylesheet" />
    <link href="../Content/site.css" rel="stylesheet" />
    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" />
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <!-- Calendario -->
    <link href="../resources/css/daterangepicker.css" rel="stylesheet" />
    <script src="../resources/css/daterangepicker.js"></script>
    <script src="../resources/css/moment.min.js"></script>
    <script>
        function ddlselect() {
            var d = document.getElementById("ddselect");
            var displaytext = d.options[d.selectedIndex].text;
            document.getElementById("txtHorReserva").value = displaytext;
        }
    </script>
</head>
<body>
    <header>
        <!-- Navbar-->
        <nav class="navbar navbar-expand-sm bg-dark navbar-dark fixed-top">
            <a class="navbar-brand" href="Login.aspx">Easy Restaurant</a>
            <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" href="ClienteOpciones.aspx">Volver</a>
            </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Contacto</a>
                </li>
            </ul>
        </nav>
    </header>
    <form id="form1" runat="server">
        <style>
            /* NOTE: The styles were added inline because Prefixfree needs access to your styles and they must be inlined if they are on local disk! */
            body {
                background: url('https://d1ralsognjng37.cloudfront.net/bbbde3f7-8508-486a-95bf-119bc120662f.jpeg') no-repeat center center fixed;
                -webkit-background-size: cover;
                -moz-background-size: cover;
                background-size: cover;
                -o-background-size: cover;
            }
        </style>
        <br />
        <br />
        <div class="container">
            <div class="card border-0 shadow my-5">
                <div class="card-body p-5">
                    <h1 class="font-weight-light">Formulario de Reserva</h1>
                    <br />
                    <!-- Formulario de Reserva -->
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="exampleInputEmail1">Nombre y Apellido</label>
                            <asp:TextBox class="form-control" ID="txtNombreReserva" runat="server" placeholder="Nombre Apellido"></asp:TextBox>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="exampleInputPassword1">Rut (Con puntos y Guión)</label>
                            <asp:TextBox class="form-control" ID="txtRutReserva" runat="server" placeholder="11.111.111-1"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="exampleInputEmail1">Teléfono</label>
                            <asp:TextBox class="form-control" ID="txtTelefonoReserva" runat="server" placeholder="9 56748382"></asp:TextBox>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="exampleInputPassword1">E-mail</label>
                            <asp:TextBox class="form-control" ID="txtEmailReserva" runat="server" placeholder="email@email.com"></asp:TextBox>
                        </div>
                    </div>
                    <!-- ultima linea de forma -->
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="exampleInputPassword1">Ingrese Fecha</label>
                            <asp:TextBox type="date" class="form-control" ID="txtFechaReserva" runat="server"></asp:TextBox>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="exampleInputPassword1">Horario</label>
                            <select id="ddselect" onchange="ddlselect();" class="form-control">
                                <option>Selecciona un Horario</option>
                                <option value="13:00">13:00</option>
                                <option value="14:00">14:00</option>
                                <option value="15:00">15:00</option>
                                <option value="15:00">16:00</option>
                                <option value="15:00">17:00</option>
                                <option value="15:00">18:00</option>
                                <option value="15:00">19:00</option>
                                <option value="15:00">20:00</option>
                                <option value="15:00">21:00</option>
                                <option value="15:00">22:00</option>
                            </select>
                          </div>
                        <div class="form-group col-md-2">
                            <label for="exampleInputPassword1">Selección</label>
                            <asp:TextBox class="form-control" ID="txtHorReserva" runat="server"></asp:TextBox>
                        </div>
                            

                    </div>
                    <asp:Button class="btn btn-primary" ID="btnGuardarReserva" runat="server" Text="Reservar" OnClick="btnGuardarReserva_Click" />
                    <asp:Button class="btn btn-secondary" ID="btnVerReservas" runat="server" Text="Ver Reservas" OnClick="btnVerReservas_Click"/>
                    <div style="height: 130px"></div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
