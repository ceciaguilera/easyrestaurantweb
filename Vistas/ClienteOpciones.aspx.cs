﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace EasyRestaurantWeb.Vistas
{
    public partial class ClienteOpciones : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnMesa_Click(object sender, EventArgs e)
        {
            Response.Redirect("SolicMesa.aspx");
        }

        protected void btnReserva_Click(object sender, EventArgs e)
        {
            Response.Redirect("ClienteReservar.aspx");
        }

        protected void ButtobtnCancelar_Click(object sender, EventArgs e)
        {
            Response.Redirect("ClienteCancelarR.aspx");
        }

        protected void btnPedido_Click(object sender, EventArgs e)
        {
            Response.Redirect("ClientePedido.aspx");
        }
    }
}