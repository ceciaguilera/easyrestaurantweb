﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls;
using System.Data.OracleClient;
using System.Windows.Forms;
using System.Data;

namespace EasyRestaurantWeb.Vistas
{
    public partial class ClienteOrdActual : System.Web.UI.Page
    {
        OracleConnection conexion = new OracleConnection("DATA SOURCE = ORCL ; PASSWORD = rs2020 ; USER ID = restauranteasy");

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                FillTabla();
            }
        }

        private void FillTabla()
        {
            conexion.Open();
            OracleCommand comando = new OracleCommand("SP_GET_ORDENCOCINA", conexion);
            comando.CommandType = System.Data.CommandType.StoredProcedure;
            comando.Parameters.Add("registroOrdenCocina", OracleType.Cursor).Direction = ParameterDirection.Output;
            OracleDataAdapter adaptador = new OracleDataAdapter();
            adaptador.SelectCommand = comando;
            DataTable tabla = new DataTable();
            adaptador.Fill(tabla);
            gridPedidoActual.DataSource = tabla;
            gridPedidoActual.DataBind();
        }

        protected void btnPedidoAdicional_Click(object sender, EventArgs e)
        {
            Response.Redirect("ClientePedido.aspx");
        }
    }
}