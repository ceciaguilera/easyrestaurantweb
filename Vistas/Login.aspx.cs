﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//using Oracle.DataAccess.Client;
using System.Data.OracleClient;
using System.Windows.Forms;
using System.Data;
//using Oracle.DataAccess.Client;

namespace EasyRestaurantWeb.Vistas
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        OracleConnection conexion = new OracleConnection("DATA SOURCE = ORCL ; PASSWORD = rs2020 ; USER ID = restauranteasy");

        protected void btnInicioSesion_Click(object sender, EventArgs e)
        {
            conexion.Open();
            OracleCommand comando = new OracleCommand("SP_GET_LOGIN", conexion);
            comando.CommandType = System.Data.CommandType.StoredProcedure;

            comando.Parameters.Add("rut_u", OracleType.VarChar).Value = txtRut.Text;
            comando.Parameters.Add("clave_u", OracleType.VarChar).Value = txtPass.Text;
            comando.Parameters.Add("SALIDA", OracleType.Cursor).Direction = ParameterDirection.Output;

            comando.ExecuteNonQuery();

            OracleDataReader lector = comando.ExecuteReader();

            if (lector.Read())
            {
                if (lector["TIPOUSUARIO"].ToString() == "Cliente")
                {
                    Server.Transfer("ClienteOpciones.aspx");
                }
                if (lector["TIPOUSUARIO"].ToString() == "Cocina")
                {
                    Server.Transfer("Cocina/CocinaOrden.aspx");
                }
                if (lector["TIPOUSUARIO"].ToString() == "Garzon")
                {
                    Server.Transfer("Garzon/GarzonOpciones.aspx");
                }
                if (lector["TIPOUSUARIO"].ToString() == "Caja")
                {
                    Server.Transfer("Caja/CajaPedido.aspx");
                }
            }
        }

        protected void txtPass_TextChanged(object sender, EventArgs e)
        {

        }
    }
}