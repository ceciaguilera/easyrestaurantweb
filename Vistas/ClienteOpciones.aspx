﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ClienteOpciones.aspx.cs" Inherits="EasyRestaurantWeb.Vistas.ClienteOpciones" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <!-- CSS -->
    <link href="../resources/css/estilos.css" rel="stylesheet" />
    <link href="../Content/site.css" rel="stylesheet" />
    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" />
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" ></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>    
</head>
<body>
    <!-- Navbar-->
    <nav class="navbar navbar-expand-sm bg-dark navbar-dark fixed-top">
        <a class="navbar-brand" href="Login.aspx">Easy Restaurant</a>
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" href="ClienteOpciones.aspx">Volver</a>
            </li>
             <li class="nav-item">
                <a class="nav-link" href="#">Contacto</a>
            </li>
        </ul>
    </nav>
    <form id="form1" runat="server">
        <style>
            /* NOTE: The styles were added inline because Prefixfree needs access to your styles and they must be inlined if they are on local disk! */
            .masthead {
                height: 100vh;
                min-height: 500px;
                background-image: url('https://source.unsplash.com/BtbjCFUvBXs/1920x1080');
                background-size: cover;
                background-position: center;
                background-repeat: no-repeat;
            }
        </style>
        <header class="masthead">
            <div class="container h-100">
                <div class="row h-100 align-items-center">
                    <div class="col-12 text-center">
                        <h1 class="font-weight-light">Bienvenido a Easy Restaurant</h1>
                        <br />
                        <asp:Button class="btn btn-dark" ID="btnReserva" runat="server" Text="Crear Reserva" OnClick="btnReserva_Click" />
                        <asp:Button class="btn btn-dark" ID="ButtobtnCancelar" runat="server" Text="Cancelar Reserva" OnClick="ButtobtnCancelar_Click" />
                        <asp:Button class="btn btn-info" ID="btnMesa" runat="server" Text="Seleccionar Mesa" OnClick="btnMesa_Click" />
                        <asp:Button class="btn btn-info" ID="btnPedido" runat="server" Text="Realizar Pedido" OnClick="btnPedido_Click"/>
                    </div>
                </div>
            </div>
        </header>
    </form>
</body>
</html>
