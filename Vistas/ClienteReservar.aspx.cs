﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.OracleClient;
//using Oracle.DataAccess.Client;
using System.Windows.Forms;


namespace EasyRestaurantWeb.Vistas
{
    public partial class ClienteReservar : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        OracleConnection conexion = new OracleConnection("DATA SOURCE = ORCL ; PASSWORD = rs2020 ; USER ID = restauranteasy");

        protected void btnGuardarReserva_Click(object sender, EventArgs e)
        {
            conexion.Open();
            OracleCommand comando = new OracleCommand("SP_CREAR_RESERVA", conexion);

            comando.CommandType = System.Data.CommandType.StoredProcedure;
            comando.Parameters.Add("CLIENTE_R", OracleType.VarChar).Value = (txtNombreReserva.Text);
            comando.Parameters.Add("RUT_R", OracleType.VarChar).Value = (txtRutReserva.Text);
            comando.Parameters.Add("TELEFONO_R", OracleType.VarChar).Value = (txtTelefonoReserva.Text);
            comando.Parameters.Add("EMAIL_R", OracleType.VarChar).Value = (txtEmailReserva.Text);
            comando.Parameters.Add("FECHA_R", OracleType.VarChar).Value = (txtFechaReserva.Text);
            comando.Parameters.Add("HORARIO_R", OracleType.VarChar).Value = (txtHorReserva.Text);
            comando.ExecuteNonQuery();
            MessageBox.Show("Reserva Realizada");

            conexion.Close();

            //Limpiar Datos
            txtNombreReserva.Text = "";
            txtRutReserva.Text = "";
            txtTelefonoReserva.Text = "";
            txtEmailReserva.Text = "";
            txtFechaReserva.Text = "";
            txtHorReserva.Text = "";
        }

        protected void btnVerReservas_Click(object sender, EventArgs e)
        {
            Response.Redirect("ClienteCancelarR.aspx");
        }
    }
}