﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ClientePagarCuenta.aspx.cs" Inherits="EasyRestaurantWeb.Vistas.ClientePagarCuenta" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <!-- CSS -->
    <link href="../resources/css/estilos.css" rel="stylesheet" />
    <link href="../Content/site.css" rel="stylesheet" />
    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" />
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</head>
<body>
    <header>
        <!-- Navbar-->
        <nav class="navbar navbar-expand-sm bg-dark navbar-dark fixed-top">
            <a class="navbar-brand" href="Login.aspx">Easy Restaurant</a>
            <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" href="ClienteOrdActual.aspx">Volver</a>
            </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Contacto</a>
                </li>
            </ul>
        </nav>
    </header>
    <form id="form1" runat="server">
        <style>
            /* NOTE: The styles were added inline because Prefixfree needs access to your styles and they must be inlined if they are on local disk! */
            body {
                background: url('https://d1ralsognjng37.cloudfront.net/bbbde3f7-8508-486a-95bf-119bc120662f.jpeg') no-repeat center center fixed;
                -webkit-background-size: cover;
                -moz-background-size: cover;
                background-size: cover;
                -o-background-size: cover;
            }
        </style>
        <br />
        <br />
        <div class="container">
            <div class="card border-0 shadow my-5">
                <div class="card-body p-5">
                    <h1 class="font-weight-light">Revisar Detalle Boleta</h1>
                    <br />
                    <!-- Tabla-->
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Mesa</th>
                                <th>Pedido</th>
                                <th>Precio C/U</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Mesa 1</td>
                                <td>Chorrillana/huevo
                                </td>
                                <th>$5.990
                                </th>
                            </tr>
                            <tr>
                                <td></td>
                                <td>Coca Cola Light
                                </td>
                                <th>$2.000
                                </th>
                            </tr>
                            <tr>
                                <td></td>
                                <td>Coca Cola Light
                                </td>
                                <th>$2.000
                                </th>
                            </tr>
                            <tr>
                                <td></td>
                                <td>TOTAL
                                </td>
                                <th>$9.990
                                </th>
                            </tr>
                             <tr>
                                <td></td>
                                <td>Propina Sugerida (10%)
                                </td>
                                <th>$990
                                </th>
                            </tr>
                             <tr>
                                <td></td>
                                <td>TOTAL FINAL
                                </td>
                                <th>$10.980
                                </th>
                            </tr>
                        </tbody>
                    </table>
                    <div class="col-12 text-center">
                        <!-- Button trigger modal -->
                        <button type="button" class="btn btn-secondary" data-toggle="modal" data-target="#exampleModal">
                            Pagar Cuenta Online
                        </button>
                        <!-- Modal -->
                        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Pago Online</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        Opciones de Pago: Webpay - OnePay - MercadoPago - Banco de Chile
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Elige una Opción</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br />
                    <div class="col-12 text-center">
                        <!-- Button trigger modal -->
                        <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#exampleModal2">
                            Pagar Cuenta Presencial
                        </button>
                        <!-- Modal -->
                        <div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel2">Pago Presencial</h5>
                                    </div>
                                    <div class="modal-body">
                                        Se envío una Notificación a tu Garzón. Se acercará a tu Mesa para Pagar en Efectivo o RedCompra.
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="height: 200px"></div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
