﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.OracleClient;
using System.Windows.Forms;
using System.Data;

namespace EasyRestaurantWeb.Vistas
{
    public partial class SolicMesa : System.Web.UI.Page
    {
        OracleConnection conexion = new OracleConnection("DATA SOURCE = ORCL ; PASSWORD = rs2020 ; USER ID = restauranteasy");

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                FillTabla();
            }

        }

        private void FillTabla()
        {
            conexion.Open();
            OracleCommand comando = new OracleCommand("SP_GET_MESAS", conexion);
            comando.CommandType = System.Data.CommandType.StoredProcedure;
            comando.Parameters.Add("registroMesas", OracleType.Cursor).Direction = ParameterDirection.Output;
            OracleDataAdapter adaptador = new OracleDataAdapter();
            adaptador.SelectCommand = comando;
            DataTable tabla = new DataTable();
            adaptador.Fill(tabla);
            gridMesas.DataSource = tabla;
            gridMesas.DataBind();
            conexion.Close();
        }

        protected void btnReservarMesa_Click(object sender, EventArgs e)
        {
            foreach (GridViewRow row in gridMesas.Rows)
            {
                System.Web.UI.WebControls.CheckBox cbox = (row.Cells[0].FindControl("chkSelectMesa") as System.Web.UI.WebControls.CheckBox);
                int rollno = Convert.ToInt32(row.Cells[1].Text);
                if (cbox.Checked)
                {
                    updaterow(rollno);
                }
            }
            gridMesas.DataBind();

        }

        private void updaterow(int rollno)
        {
            String updatedata = "UPDATE MESAS SET estado_mesa = 'Ocupada' WHERE estado_mesa = 'Disponible' and id_mesa = " + rollno;
            OracleCommand cmd = new OracleCommand(updatedata, conexion);
            conexion.Open();
            cmd.CommandText = updatedata;
            cmd.ExecuteNonQuery();
            MessageBox.Show("Mesa Reservada con Exito. Favor dirijase a la Mesa Seleccionada");
        }

        protected void btnActualizarMesas_Click(object sender, EventArgs e)
        {
            conexion.Open();
            OracleCommand comando = new OracleCommand("SP_GET_MESAS", conexion);
            comando.CommandType = System.Data.CommandType.StoredProcedure;
            comando.Parameters.Add("registroMesas", OracleType.Cursor).Direction = ParameterDirection.Output;
            OracleDataAdapter adaptador = new OracleDataAdapter();
            adaptador.SelectCommand = comando;
            DataTable tabla = new DataTable();
            adaptador.Fill(tabla);
            gridMesas.DataSource = tabla;
            gridMesas.DataBind();
            conexion.Close();

        }
    }
}