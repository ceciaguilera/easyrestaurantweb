﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Data.OracleClient;
using System.Windows.Forms;
using System.Data;
using System.Windows.Forms.ComponentModel;
using System.Web.UI.WebControls;

namespace EasyRestaurantWeb.Vistas
{
    public partial class ClientePedido : System.Web.UI.Page
    {
        OracleConnection conexion = new OracleConnection("DATA SOURCE = ORCL ; PASSWORD = rs2020 ; USER ID = restauranteasy");

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                FillTabla();
            } 
        }

        private void FillTabla()
        {
            //PLATO DE FONDO
            conexion.Open();
            OracleCommand comando = new OracleCommand("SP_GET_PRODUCTOS_PLATOF", conexion);
            comando.CommandType = System.Data.CommandType.StoredProcedure;
            comando.Parameters.Add("registroProdMenu", OracleType.Cursor).Direction = ParameterDirection.Output;
            OracleDataAdapter adaptador = new OracleDataAdapter();
            adaptador.SelectCommand = comando;
            DataTable tabla = new DataTable();
            adaptador.Fill(tabla);
            gridPlatoF.DataSource = tabla;
            gridPlatoF.DataBind();

            //ACOMPAÑAMIENTO
            OracleCommand comando2 = new OracleCommand("SP_GET_PRODUCTOS_ACOMP", conexion);
            comando2.CommandType = System.Data.CommandType.StoredProcedure;
            comando2.Parameters.Add("registroProdAcom", OracleType.Cursor).Direction = ParameterDirection.Output;
            adaptador.SelectCommand = comando2;
            DataTable tabla2 = new DataTable();
            adaptador.Fill(tabla2);
            gridAcomp.DataSource = tabla2;
            gridAcomp.DataBind();

            //BEBESTIBLE
            OracleCommand comando3 = new OracleCommand("SP_GET_PRODUCTOS_BEBEST", conexion);
            comando3.CommandType = System.Data.CommandType.StoredProcedure;
            comando3.Parameters.Add("registroProdBeb", OracleType.Cursor).Direction = ParameterDirection.Output;
            adaptador.SelectCommand = comando3;
            DataTable tabla3 = new DataTable();
            adaptador.Fill(tabla3);
            gridBebestib.DataSource = tabla3;
            gridBebestib.DataBind();

            //ENSALADA
            OracleCommand comando4 = new OracleCommand("SP_GET_PRODUCTOS_SALAD", conexion);
            comando4.CommandType = System.Data.CommandType.StoredProcedure;
            comando4.Parameters.Add("registroProdSal", OracleType.Cursor).Direction = ParameterDirection.Output;
            adaptador.SelectCommand = comando4;
            DataTable tabla4 = new DataTable();
            adaptador.Fill(tabla4);
            gridSalad.DataSource = tabla4;
            gridSalad.DataBind();

            //POSTRE
            OracleCommand comando5 = new OracleCommand("SP_GET_PRODUCTOS_POSTRE", conexion);
            comando5.CommandType = System.Data.CommandType.StoredProcedure;
            comando5.Parameters.Add("registroProdPos", OracleType.Cursor).Direction = ParameterDirection.Output;
            adaptador.SelectCommand = comando5;
            DataTable tabla5 = new DataTable();
            adaptador.Fill(tabla5);
            gridPostre.DataSource = tabla5;
            gridPostre.DataBind();

            conexion.Close();
        }

        protected void btnRealizarPedido_Click(object sender, EventArgs e)
        {
            String pedido = 
                "Plato de Fondo: " + lbl1.Text + "\n" + 
                "Acompañamiento: " + lbl2.Text + "\n" + 
                "Bebestible: "+ lbl3.Text + "\n" + 
                "Ensalada: "+ lbl4.Text + "\n" + 
                "Postre: "+ lbl5.Text + "\n";

            conexion.Open();
            OracleCommand comando = new OracleCommand("SP_CREAR_ORDENES", conexion);

            comando.CommandType = System.Data.CommandType.StoredProcedure;
            comando.Parameters.Add("FECHA_OR", OracleType.VarChar).Value = "18/10/2020";
            comando.Parameters.Add("ESTADO_OR", OracleType.VarChar).Value = "Creada";
            comando.Parameters.Add("CANTIDAD_OR", OracleType.VarChar).Value = "1";
            comando.Parameters.Add("COMENTARIO_OR", OracleType.VarChar).Value = (txtComentario.Text);
            comando.Parameters.Add("PRODUCTO_OR", OracleType.VarChar).Value = pedido;
            comando.Parameters.Add("PRECIO_OR", OracleType.VarChar).Value = "null";
            comando.ExecuteNonQuery();
            MessageBox.Show("Orden Realizada con Exito. Se direccionará a su Orden Actual");
            conexion.Close();
            Response.Redirect("ClienteOrdActual.aspx");
        }

        protected void chk_chackedchange(object sender, EventArgs e)
        {
            //PLATO DE FONDO
            int rowind = ((GridViewRow)(sender as System.Web.UI.Control).NamingContainer).RowIndex;
            lbl1.Text = gridPlatoF.Rows[rowind].Cells[1].Text;
            lblP1.Text = gridPlatoF.Rows[rowind].Cells[2].Text;
        }

        protected void chk_chackedchangeAcomp(object sender, EventArgs e)
        {
            //ACOMPAÑAMIENTO
            int rowind = ((GridViewRow)(sender as System.Web.UI.Control).NamingContainer).RowIndex;
            lbl2.Text = gridAcomp.Rows[rowind].Cells[1].Text;
            lblP2.Text = gridAcomp.Rows[rowind].Cells[2].Text;
        }

        protected void chkSeleccionarBebest(object sender, EventArgs e)
        {
            //BEBESTIBLE
            int rowind3 = ((GridViewRow)(sender as System.Web.UI.Control).NamingContainer).RowIndex;
            lbl3.Text = gridBebestib.Rows[rowind3].Cells[1].Text;
            lblP3.Text = gridBebestib.Rows[rowind3].Cells[2].Text;
        }

        protected void chk_chkSeleccionarEnsal(object sender, EventArgs e)
        {
            //ENSALADA
            int rowind4 = ((GridViewRow)(sender as System.Web.UI.Control).NamingContainer).RowIndex;
            lbl4.Text = gridSalad.Rows[rowind4].Cells[1].Text;
            lblP4.Text = gridSalad.Rows[rowind4].Cells[2].Text;
        }

        protected void chk_chkSeleccionarPostr(object sender, EventArgs e)
        {
            //POSTRE
            int rowind5 = ((GridViewRow)(sender as System.Web.UI.Control).NamingContainer).RowIndex;
            lbl5.Text = gridPostre.Rows[rowind5].Cells[1].Text;
            lblP5.Text = gridPostre.Rows[rowind5].Cells[2].Text;
        }

        protected void gridPlatoF_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}