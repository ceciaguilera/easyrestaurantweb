﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ClientePedido.aspx.cs" Inherits="EasyRestaurantWeb.Vistas.ClientePedido" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <!-- CSS -->
    <link href="../resources/css/estilos.css" rel="stylesheet" />
    <link href="../Content/site.css" rel="stylesheet" />
    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" />
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</head>
<body>
    <header>
        <!-- Navbar-->
        <nav class="navbar navbar-expand-sm bg-dark navbar-dark fixed-top">
            <a class="navbar-brand" href="Login.aspx">Easy Restaurant</a>
            <ul class="navbar-nav">
                            <li class="nav-item">
                <a class="nav-link" href="ClienteOpciones.aspx">Volver</a>
            </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Contacto</a>
                </li>
            </ul>
        </nav>
    </header>
    <form id="form1" runat="server">
        <style>
            /* NOTE: The styles were added inline because Prefixfree needs access to your styles and they must be inlined if they are on local disk! */
            body {
                background: url('https://d1ralsognjng37.cloudfront.net/bbbde3f7-8508-486a-95bf-119bc120662f.jpeg') no-repeat center center fixed;
                -webkit-background-size: cover;
                -moz-background-size: cover;
                background-size: cover;
                -o-background-size: cover;
            }
        </style>
        <br />
        <br />
        <div class="container">
            <div class="card border-0 shadow my-5">
                <div class="card-body p-5">
                    <h1 class="font-weight-light">Realizar Orden</h1>
                    <br />
                    <asp:Button class="btn btn-dark" ID="btnRealizarPedido" runat="server" Text="Realizar Pedido" OnClick="btnRealizarPedido_Click" />
                    <asp:Button class="btn btn-warning" ID="btnORdenActual" runat="server" Text="Ver mi Orden Actual" OnClick="btnRealizarPedido_Click" />
                    <br />
                    <br />
                    <h3 class="font-weight-light">Aquí aparecerá el Resumen de tu Orden</h3>
                    <br />
                    <asp:Panel ID="Panel1" runat="server" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px">
                        <asp:Label ID="lbl1" runat="server" Text=""></asp:Label>
                        <asp:Label ID="lblP1" runat="server" Text=""></asp:Label>
                        <br />
                        <asp:Label ID="lbl2" runat="server" Text=""></asp:Label>
                        <asp:Label ID="lblP2" runat="server" Text=""></asp:Label>
                        <br />
                        <asp:Label ID="lbl3" runat="server" Text=""></asp:Label>
                        <asp:Label ID="lblP3" runat="server" Text=""></asp:Label>
                        <br />
                        <asp:Label ID="lbl4" runat="server" Text=""></asp:Label>
                        <asp:Label ID="lblP4" runat="server" Text=""></asp:Label>
                        <br />
                        <asp:Label ID="lbl5" runat="server" Text=""></asp:Label>
                        <asp:Label ID="lblP5" runat="server" Text=""></asp:Label>
                    </asp:Panel>
                    <br />
                    <br />
                    <h3 class="font-weight-light">Dejar comentarios de tu Orden</h3>
                    <br />
                    <asp:TextBox ID="txtComentario" runat="server" Height="118px" Width="1018px"></asp:TextBox>
                    <br />
                    <br />
                    <h1 class="font-weight-light">Selecciona tus Pedidos</h1>
                    <br />
                    <div class="form-inline">
                        <input class="form-control mr-sm-2" type="search" placeholder="Buscar" aria-label="Buscar Producto" />
                        <asp:Button class="btn btn-warning" ID="btnBuscarProducto" runat="server" Text="Buscar Producto" />
                    </div>
                    <br />
                    <h2 class="font-weight-light">Plato de Fondo</h2>
                    <!-- Tabla Plato de Fondo -->
                    <div>
                        <asp:GridView ID="gridPlatoF" runat="server" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="4" ForeColor="Black" GridLines="Horizontal" Width="1018px" OnSelectedIndexChanged="gridPlatoF_SelectedIndexChanged">
                            <FooterStyle BackColor="#CCCC99" ForeColor="Black" />
                            <HeaderStyle BackColor="#333333" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Right" />
                            <SelectedRowStyle BackColor="#CC3333" Font-Bold="True" ForeColor="White" />
                            <SortedAscendingCellStyle BackColor="#F7F7F7" />
                            <SortedAscendingHeaderStyle BackColor="#4B4B4B" />
                            <SortedDescendingCellStyle BackColor="#E5E5E5" />
                            <SortedDescendingHeaderStyle BackColor="#242121" />
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkSeleccionarPlatoF" runat="server" AutoPostBack="True" OnCheckedChanged="chk_chackedchange" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                    <br />
                    <br />
                    <br />
                    <h2 class="font-weight-light">Acompañamiento</h2>
                    <!-- Tabla Acompañamiento -->
                    <div>
                        <asp:GridView ID="gridAcomp" runat="server" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="4" ForeColor="Black" GridLines="Horizontal" Width="1018px" OnSelectedIndexChanged="chk_chackedchangeAcomp">
                            <FooterStyle BackColor="#CCCC99" ForeColor="Black" />
                            <HeaderStyle BackColor="#333333" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Right" />
                            <SelectedRowStyle BackColor="#CC3333" Font-Bold="True" ForeColor="White" />
                            <SortedAscendingCellStyle BackColor="#F7F7F7" />
                            <SortedAscendingHeaderStyle BackColor="#4B4B4B" />
                            <SortedDescendingCellStyle BackColor="#E5E5E5" />
                            <SortedDescendingHeaderStyle BackColor="#242121" />
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkSeleccionarAcomp" runat="server" AutoPostBack="True" OnCheckedChanged="chk_chackedchangeAcomp" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                    <br />
                    <br />
                    <br />
                    <h2 class="font-weight-light">Bebestible</h2>
                    <!-- Tabla Bebestible -->
                    <div>
                        <asp:GridView ID="gridBebestib" runat="server" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="4" ForeColor="Black" GridLines="Horizontal" Width="1018px" OnSelectedIndexChanged="chkSeleccionarBebest">
                            <FooterStyle BackColor="#CCCC99" ForeColor="Black" />
                            <HeaderStyle BackColor="#333333" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Right" />
                            <SelectedRowStyle BackColor="#CC3333" Font-Bold="True" ForeColor="White" />
                            <SortedAscendingCellStyle BackColor="#F7F7F7" />
                            <SortedAscendingHeaderStyle BackColor="#4B4B4B" />
                            <SortedDescendingCellStyle BackColor="#E5E5E5" />
                            <SortedDescendingHeaderStyle BackColor="#242121" />
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkSeleccionarBebest" runat="server" AutoPostBack="True" OnCheckedChanged="chkSeleccionarBebest" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                    <br />
                    <br />
                    <br />
                    <h2 class="font-weight-light">Ensaladas</h2>
                    <!-- Tabla Ensaladas -->
                    <div>
                        <asp:GridView ID="gridSalad" runat="server" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="4" ForeColor="Black" GridLines="Horizontal" Width="1018px" OnSelectedIndexChanged="chk_chkSeleccionarEnsal">
                            <FooterStyle BackColor="#CCCC99" ForeColor="Black" />
                            <HeaderStyle BackColor="#333333" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Right" />
                            <SelectedRowStyle BackColor="#CC3333" Font-Bold="True" ForeColor="White" />
                            <SortedAscendingCellStyle BackColor="#F7F7F7" />
                            <SortedAscendingHeaderStyle BackColor="#4B4B4B" />
                            <SortedDescendingCellStyle BackColor="#E5E5E5" />
                            <SortedDescendingHeaderStyle BackColor="#242121" />
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkSeleccionarEnsal" runat="server" AutoPostBack="True" OnCheckedChanged="chk_chkSeleccionarEnsal" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                    <br />
                    <br />
                    <br />
                    <h2 class="font-weight-light">Postre</h2>
                    <!-- Tabla Postre -->
                    <div>
                        <asp:GridView ID="gridPostre" runat="server" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="4" ForeColor="Black" GridLines="Horizontal" Width="1018px" OnSelectedIndexChanged="chk_chkSeleccionarPostr">
                            <FooterStyle BackColor="#CCCC99" ForeColor="Black" />
                            <HeaderStyle BackColor="#333333" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Right" />
                            <SelectedRowStyle BackColor="#CC3333" Font-Bold="True" ForeColor="White" />
                            <SortedAscendingCellStyle BackColor="#F7F7F7" />
                            <SortedAscendingHeaderStyle BackColor="#4B4B4B" />
                            <SortedDescendingCellStyle BackColor="#E5E5E5" />
                            <SortedDescendingHeaderStyle BackColor="#242121" />
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkSeleccionarPostr" runat="server" AutoPostBack="True" OnCheckedChanged="chk_chkSeleccionarPostr" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                    <br />
                    <br />

                </div>
                <div style="height: 400px"></div>
                <br />
            </div>
        </div>
        </div>
        </div>
    </form>
</body>
</html>
