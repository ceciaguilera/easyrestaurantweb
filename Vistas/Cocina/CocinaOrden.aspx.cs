﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.OracleClient;
using System.Windows.Forms;
using System.Data;
using System.Windows.Forms.ComponentModel;

namespace EasyRestaurantWeb.Vistas
{
    public partial class CocinaOrden : System.Web.UI.Page
    {
        OracleConnection conexion = new OracleConnection("DATA SOURCE = ORCL ; PASSWORD = rs2020 ; USER ID = restauranteasy");
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                FillTabla();
            }
        }

        private void FillTabla()
        {
            conexion.Open();
            OracleCommand comando = new OracleCommand("SP_GET_ORDENCOCINA", conexion);
            comando.CommandType = System.Data.CommandType.StoredProcedure;
            comando.Parameters.Add("registroOrdenCocina", OracleType.Cursor).Direction = ParameterDirection.Output;
            OracleDataAdapter adaptador = new OracleDataAdapter();
            adaptador.SelectCommand = comando;
            DataTable tabla = new DataTable();
            adaptador.Fill(tabla);
            gridOrdenCocina.DataSource = tabla;
            gridOrdenCocina.DataBind();
        }

        protected void btnTerminarOrden_Click(object sender, EventArgs e)
        {
            foreach (GridViewRow row in gridOrdenCocina.Rows)
            {
                System.Web.UI.WebControls.CheckBox cbox = (row.Cells[0].FindControl("chkTerminarOrden") as System.Web.UI.WebControls.CheckBox);
                int rollno = Convert.ToInt32(row.Cells[1].Text);
                if (cbox.Checked)
                {
                    updateRow(rollno);
                }
            }
            gridOrdenCocina.DataBind();
        }

        private void updateRow(int rollno)
        {
            String updatedata = "UPDATE ORDENES SET ESTADO_ORDEN = 'Terminada' WHERE ID_ORDEN = " + rollno;
            OracleCommand cmd = new OracleCommand(updatedata, conexion);
            conexion.Open();
            cmd.CommandText = updatedata;
            cmd.ExecuteNonQuery();
            MessageBox.Show("Orden Terminada");
        }

        protected void btnOrdenesPendientes_Click(object sender, EventArgs e)
        {
            conexion.Open();
            OracleCommand comando = new OracleCommand("SP_GET_ORDENCOCINA", conexion);
            comando.CommandType = System.Data.CommandType.StoredProcedure;
            comando.Parameters.Add("registroOrdenCocina", OracleType.Cursor).Direction = ParameterDirection.Output;
            OracleDataAdapter adaptador = new OracleDataAdapter();
            adaptador.SelectCommand = comando;
            DataTable tabla = new DataTable();
            adaptador.Fill(tabla);
            gridOrdenCocina.DataSource = tabla;
            gridOrdenCocina.DataBind();
        }

        protected void btnOrdenesTerminadas_Click(object sender, EventArgs e)
        {
            conexion.Open();
            OracleCommand comando = new OracleCommand("SP_GET_ORDENCOCINA_TERMINADA", conexion);
            comando.CommandType = System.Data.CommandType.StoredProcedure;
            comando.Parameters.Add("registroOrdenCocinaT", OracleType.Cursor).Direction = ParameterDirection.Output;
            OracleDataAdapter adaptador = new OracleDataAdapter();
            adaptador.SelectCommand = comando;
            DataTable tabla = new DataTable();
            adaptador.Fill(tabla);
            gridOrdenCocina.DataSource = tabla;
            gridOrdenCocina.DataBind();
        }
    }
}